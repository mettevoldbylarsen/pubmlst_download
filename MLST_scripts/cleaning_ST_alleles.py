#! /usr/local/python/bin/python2.4

#Dette program skal bruges ved cleanup af ST alleles filer

import re
import sys
import os

datadir = '/home'

sys.stderr = open( datadir + '/data/errors.alleles1', 'a') #Unforseen problems making the program crash should be written to a file named errors.alleles1

fhErrors = open( datadir + '/data/errors.alleles2', 'a') #Forseen problems should be written to a file named errors.alleles2



#Declaring variables
seq = ""
errorTester =  1
previousHeaderLine = ""
wholeSeq = ""


#In the input file, noGenes, each profile file is listed along with the number og genes for the species.
inputfil = datadir + "/data/noGenes.txt"
fh = open(inputfil,'r')

for line in fh.readlines():                   #Each line has info about one species
    line = line.strip()
    patternLine = re.compile('(\w+)\.txt:\s+(\d+)')  #re for the lines in noGenes
    species = patternLine.search(line).group(1)  #The species (or more precisely the name of the MLST scheme)
    noGenes = patternLine.search(line).group(2)  #No of genes for this MLST scheme
    #print "Species: " + species
    
    fileName = species + ".txt"   #Filename of species profile file
    pathFilename = datadir + "/profiles/" + fileName          #Path and filename of species profile file
    fh2 = open(pathFilename, 'r')      #Here we open the species profile file
    firstLineProfile = fh2.readlines()[0] #First line in species profile file is ^ST(\tGeneName)*
    firstLineProfile = firstLineProfile.strip()
    split_firstLineProfile = firstLineProfile.split("\t")
    
    PathFileAlleles = datadir + '/data/' + species + ".fsa"  #path and name of the file that should contain all the alleles for this MSLT scheme
    fh3 = open(PathFileAlleles, 'a')
    
    i = 1
    while i <= int(noGenes):        
        if species == "mcatarrhalis": #Exception for mcarrhalis, which doesnt write glybeta the same way in allele and profile file
            #print "Her er jeg"
            if split_firstLineProfile[i] == "glyBETA":
                #print "Nu er jeg her"
                allele_glybeta = "glyBeta"
                print allele_glybeta
                seperateGeneFile = datadir + "/alleles/" + species + "/" + allele_glybeta + ".tfa"
                print seperateGeneFile
            else:
                seperateGeneFile = datadir + "/alleles/" + species + "/" + split_firstLineProfile[i] + ".tfa"
            
        else:
            seperateGeneFile = datadir + "/alleles/" + species + "/" + split_firstLineProfile[i] + ".tfa"
        try:
                fh2 = open(seperateGeneFile,'r')
                for line2 in fh2.readlines(): 
                    line2 = line2.strip()
                    patternHeader = re.compile('>(\S+)') #The pattern of headers
                    try:
                        header = patternHeader.search(line2).group(1)   #The header is isolated
                        if (species == "incf"):
                            header_list = list(header)
                            firstfour = firstfive = header_list[0] + header_list[1] + header_list[2] + header_list[3] #The first four elements
                            firstfive = header_list[0] + header_list[1] + header_list[2] + header_list[3] + header_list[4] #The first five elements
                            #print firstfive
                            if (firstfour == "FIC_"):
                                sidste = re.sub(firstfour, '', header)
                                header = "FII_C" + sidste
                            if (firstfive == "FIIY_"):
                                sidste = re.sub(firstfive, '', header)
                                header = "FII_Y" + sidste
                            if (firstfive == "FIIS_"):
                                sidste = re.sub(firstfive, '', header)
                                header = "FII_S" + sidste
                            if (firstfive == "FIIK_"):
                                sidste = re.sub(firstfive, '', header)
                                header = "FII_K" + sidste
                            #print header    
                            headerUpper = header.upper()
                        headerUpper = header.upper()
                        headerLine = ">" + headerUpper + "\n"
                        if errorTester == 0:
                            fh3.write(previousHeaderLine)
                            fh3.write(wholeSeq)
                        previousHeaderLine = headerLine
                        wholeSeq = ""
                    except (AttributeError):  #If it is not a header, it must be checked that it is DNA sequence that only contains ATCG/atcg
                        errorTester =  0
                        patternSeq = re.compile('^(([ATGCKMRYSWBVHDN]+)|([atcgkmryswbvndn]+))') #re for a DNA sequence
                        try:
                            seq = patternSeq.search(line2).group(0) + "\n"
                            wholeSeq = wholeSeq + seq
                        except (AttributeError): #If the non-header lines do not match a DNA seq, they should not be included in the output file, but the problem should be reported in the errors.alleles2 file 
                            ErrorLine = species + " have problems. Here is the line(s) that doesn't match the DNA re: " + line2 + "\n"
                            fhErrors.write(ErrorLine) 
                            errorTester = 1
        except (IOError): #If the seperateGeneFile could not be opened, this is also written to the errors.alleles2 file
                ErrorLine = "Problems opening a file: " + seperateGeneFile + ", for the species: " + species + "\n"
                fhErrors.write(ErrorLine) 
                pass
        i += 1 
    fh3.write(previousHeaderLine)
    fh3.write(wholeSeq)
    errorTester = 1
    
    fh2.close()
    fh3.close()
fhErrors.close()
fh.close()
