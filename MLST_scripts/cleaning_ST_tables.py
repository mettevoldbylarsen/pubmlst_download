#! /usr/local/python/bin/python2.4

#Dette program skal bruges ved cleanup af ST profiles filer

import re
import sys
    
inputfil = sys.argv[1]
datadir = '/home'
fh = open(inputfil,'r')
all_lines = fh.readlines()

sys.stderr = open( datadir + '/data/errors.tables', 'a') #Problems should be written to a file named errors
fhNoGenes = open( datadir + '/data/noGenes.txt' , 'a') #No of genes for each species is saved in a separate file

#For each species a seperate file with clonal information is generated. Here I just figure out the filename. The re matches the path and the stuff in the parenthesis the species
path = re.search('(\w+?)\.txt',inputfil)
species = path.group(1)
clpxFile = datadir + "/data/" + species + ".clpx"
fhclpx = open(clpxFile , 'a') #ST and CLONAL_COMPLEX is saved in separate file

#Declaring variables
noColumns = 0

inputfil = sys.argv[1]
fh = open(inputfil,'r')
all_lines = fh.readlines()

#Exception aimed at ecoli, which currently looks like the below:
#ST adk fumC    gyrB    icd mdh purA    recA    ST Complex
#1   4   2   2   4   4   4   4   None
#3   6   4   3   7   7   7   6   ST20 Cplx
lines = []
if (species == "ecoli"):
    for line in all_lines:
        line = line.replace(" Cplx","_Cplx")
        line = line.replace(" Complex","_Complex")
        lines.append(line)
    all_lines = lines     


first_line = all_lines[0]
first_line = first_line.rstrip()   #Newline is removed from end of line
first_line = first_line.upper()       #All letters are converted to upper letters

list_first_line = first_line.split()    

rest_lines = all_lines[1:]

firstLinePattern = re.compile('ST\t(\w+\t?){4,30}')    #re for the first line
match1 = firstLinePattern.match(first_line)

AlternativeFirstLinePattern = re.compile('CGST\t(\w+\t?){4,30}')    #re for the first line for IncA/C cgPMLST
match1_alt = AlternativeFirstLinePattern.match(first_line)
    
lookingForClonalComplex = re.compile('.*(CLONAL_COMPLEX|MLST_CLADE|CC|ST_COMPLEX)')    #re looking for "CLONAL_COMPLEX" in header. Remember all letters in header has been made to upper
match2 = lookingForClonalComplex.match(first_line)

lookingForSpecies= re.compile('.*(SPECIES|LINEAGE)')
match3 = lookingForSpecies.match(first_line)

if ((match1 is not None) or (match1_alt is not None)):  #First line is as expected
    if match2:    #If there is a column with a "CLONAL_COMPLEX" header
        fhclpx = open(clpxFile , 'a') #No of genes for each species is saved in seperate file
        fhclpx.write("ST\tCLONAL_COMPLEX\n")  #This is just the header of the above-mentioned seperate file
        if (species == "abaumannii"):
            noColumns = len(list_first_line) - 2
        elif (species == "abaumannii_2"):
            noColumns = len(list_first_line) - 2
        elif (match3):
            noColumns = len(list_first_line) - 2
        else:
            noColumns = len(list_first_line) - 1
        noGenes = noColumns - 1
        fhNoGenes.write("%s:\t%d\n" % (inputfil, noGenes)) #The filename and no of genes in file is saved in a seperate file
        i = 0
        while i < noColumns:
            #print noColumns
            # Exception for hparasuis, "_6PGD" needs to be converted to "6PGD"
            if (species == "hparasuis"): 
                locus = list_first_line[i]
                if (locus == "_6PGD"):
                    locus = "6PGD"
                sys.stdout.write(locus)
                sys.stdout.write("\t")
                i += 1
            # Exception for hcinaedi, "_23_RRNA" needs to be converted to "23_RRNA"
            elif (species == "hcinaedi"): 
                locus = list_first_line[i]
                if (locus == "_23S_RRNA"):
                    locus = "23S_RRNA"
                sys.stdout.write(locus)
                sys.stdout.write("\t")
                i += 1
            elif (species == "leptospira_2"): # Exception for leptospira #2, since underscores must me removed from locus names
                locus = list_first_line[i]
                if (locus == "ADK_2"):
                    locus = "ADK2"
                elif (locus == "GLMU_2"):
                    locus = "GLMU2"
                elif (locus == "ICDA_2"):
                    locus = "ICDA2"
                elif (locus == "LIPL32_2"):
                    locus = "LIPL322"
                elif (locus == "LIPL41_2"):
                    locus = "LIPL412"
                elif (locus == "MREA_2"):
                    locus = "MREA2"
                elif (locus == "PNTA_2"):
                    locus = "PNTA2"
                sys.stdout.write(locus)
                sys.stdout.write("\t")
                i += 1
            elif (species == "bhenselae"): 
                locus = list_first_line[i]
                if (locus == "_16S"):
                    locus = "16S"
                sys.stdout.write(locus)
                sys.stdout.write("\t")
                i += 1
            elif (species == "streptomyces"): 
                locus = list_first_line[i]
                if (locus == "_16S"):
                    locus = "16S"
                sys.stdout.write(locus)
                sys.stdout.write("\t")
                i += 1
            #elif (species == "mcatarrhalis"): 
            #    locus = list_first_line[i]
            #    if (locus == "glyBETA"):
            #        locus = "glyBeta"
            #    sys.stdout.write(locus)
            #    sys.stdout.write("\t")
            #    i += 1  
            else:
                sys.stdout.write(list_first_line[i])  #Jeg bruger dette udtryk i stedet for simpel print for at undgaa automatisk whitespace
                sys.stdout.write("\t")
                i += 1 
        print "\n",
    else:   #There is no column with a "CLONAL_COMPLEX" header
        noColumns = len(list_first_line)
        if (match3):
            noColumns = len(list_first_line) - 1
        noGenes = noColumns - 1
        fhNoGenes.write("%s: %d\n" % (inputfil, noGenes))
        #Exception for hparasuis, "_6PGD" needs to be converted to "6PGD"
        if (species == "hparasuis"):
            i = 0
            while i < noColumns:
                locus = list_first_line[i]
                sys.stderr.write("Vi kommer ind i elif")
                if locus == "_6PGD":
                    locus = "6PGD"
                sys.stdout.write(locus)
                sys.stdout.write("\t")
                i += 1
            print "\n",
        #For the rest of the species
        else:
            i = 0
            while i < noColumns:
                sys.stdout.write(list_first_line[i])  #Jeg bruger dette udtryk i stedet for simpel print for at undgaa automatisk whitespace
                sys.stdout.write("\t")
                i += 1 
            print "\n",
    #Now it's time to check the pattern of the rest of the lines
    restLinePattern = re.compile(r'(\d+\t?){%s}' % noColumns) #re for the rest of the lines
    AlternativeRestLinePattern = re.compile('(\d+\.\d+\t)(\d+\t?)+') #re for the incA/C cgPMLST scheme
    for line in rest_lines:
        match4 = restLinePattern.match(line)
        match4_alt = AlternativeRestLinePattern.match(line)
        if ((match4 is not None) or (match4_alt is not None)):
            line = line.rstrip() #Newline is removed from end of line
            list_line = line.split()
            j = 0
            while j < noColumns:
                sys.stdout.write(list_line[j])
                sys.stdout.write("\t")
                j += 1 
            print "\n",
            try: #Hvis der er noget i kolonnen under headeren CLONAL_COMPLEX skrives det i seperat fil
                fhclpx.write(list_line[0] + "\t" + list_line[noColumns] + "\n")
            except IndexError:   #Hvis der ikke er noget i kolonnen under headeren springes linjen over
                pass
        else:
            sys.stderr.write("Error: line doesn't match re: %s %s" % (inputfil, line))
else:        
    sys.stderr.write("Error: First line doesn't match re: %s %s" % (inputfil, first_line))

fh.close()
fhNoGenes.close()
