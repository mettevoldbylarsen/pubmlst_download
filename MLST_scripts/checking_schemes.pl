#!/usr/local/bin/perl

use strict;
use warnings;

unless (@ARGV){
	print "$0 inputfile\n";
	exit;
}

my $inputfile = $ARGV[0];
open (FIL, $inputfile) or die "Could not open inputfile\n";

#Declaring variables
my @schemes_in_config;
my $counter_missing_fsa_files = 0;
my $counter_missing_txt_clean_files = 0;
my $counter_missing_lines_in_config_file = 0;

#Checking if files exist for each of the schemes listed in the config file   
while (defined (my $line = <FIL>)){
  next if $line =~ m/^#/;	
  my @split_line = split("\t",$line);
  my $scheme_name = $split_line[0];
  push @schemes_in_config , $scheme_name; 

  my $file_fsa = "/Desktop/repos/pubmlst_download/data_mlst/" . $scheme_name . ".fsa";
  unless (-f $file_fsa){
    print "Missing fsa file: " . $scheme_name . "\n";
    $counter_missing_fsa_files += 1;
  }
  my $file_txt_clean = "/Desktop/repos/pubmlst_download/data_mlst/" . $scheme_name . ".txt.clean";
  unless (-f $file_txt_clean){
    print "Missing txt.clean file: " . $scheme_name . "\n";
    $counter_missing_txt_clean_files += 1;
  }

}

#Collecting all the .fsa files in the data_mlst folder into an array
my @fsa_files;
opendir (Dir, "/Desktop/repos/pubmlst_download/data_mlst/") or die "Could not open /Desktop/repos/pubmlst_download/data_mlst/";
my @list = readdir(Dir);
closedir(Dir);
foreach my $f (@list){
  if ($f =~ m/\.fsa$/){
    #print $f . "\n";
    my $just_scheme = substr($f,0,-4);	
    #print $just_scheme . "\n";
    push @fsa_files , $just_scheme;
  }
}

#Checking if all fsa-files are represented by scheme in the config file   
foreach my $elem (@fsa_files){
  my $flag = 0;
  foreach my $elem2 (@schemes_in_config){
    if ($elem eq $elem2){
      #print $elem . "=" . $elem2 . "\n";
      $flag = 1;
  	}
  }
  if ($flag == 0) {
  	print "Houston, we have a problem: $elem" . "\n";
    $counter_missing_lines_in_config_file += 1;
  }
}

print "\n***********SUMMARY**************\n";
print "Missing fsa files: " . $counter_missing_fsa_files . "\n";
print "Missing txt.clean files: " . $counter_missing_txt_clean_files . "\n";
print "Missing lines in config files: " .  $counter_missing_lines_in_config_file . "\n\n";

