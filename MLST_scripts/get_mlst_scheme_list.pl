#!/tools/bin/perl -w
use strict;
use Data::Dumper;
use Getopt::Long qw(:config no_ignore_case no_auto_abbrev pass_through);

# This programm updates the list of MLST schemes by adding the new MLST schemes available on pubmlst


my ($datadir, $out_dir, $cge);

&GetOptions (
	"d=s"     => \$datadir,
	"o=s"     => \$out_dir,
   "cge"     => \$cge
);


if (defined $cge) {
	 $datadir = "/home/data2/databases/external/pubmlst";
	 $out_dir = "/home/data1/services/MLST/database_archive/database_`date +%y%m%d`/";
}
if (not defined $out_dir || not defined $datadir) {
   print "Input is missing!\n";
   exit
}

# make a hash of MLST schemes and their names in the database: ------------------------------------------------------------------------------- 
system("wget -O $datadir/dbases.xml http://pubmlst.org/data/dbases.xml") or warn "wget -O $datadir/dbases.xml http://pubmlst.org/data/dbases.xml returned $!\n";
open(IN,'<',"$datadir/dbases.xml") or die "GET SCHEME ERROR problem accessing scheme list: $! \n";
print("GET SCHEMES opened scheme list!\n");

my %schemes;
my $tmp;

while(defined(my $line = <IN>)){
	chomp $line;
	
	# extract available MLST schemes databases:
	if ($line =~ m/^<species>(.*)/){
	        my $s=$1;
		$s =~ s/^\s+//; 
		$s =~ s/\s+$//;
		$tmp = $s;
		#$schemes{$tmp} =1;
	}
	
	# extract file name of MLST scheme:
	if ($line =~ m|^<url>http://pubmlst.org/data/profiles/(\w+)\.|){
		$schemes{$tmp} = $1;
	}
}
close IN;


# add the ones downloaded from different places: ----------------------------------------------------------------------------------------------

$schemes{"Listeria"}="listeria";
$schemes{"Lactococcus lactis"}="llactis";

# make the list: ------------------------------------------------------------------------------------------------------------------------------

my @list;
my $c=0;

print Dumper(\%schemes);

while(my($key,$value) = each %schemes){
	$list[$c]=join("\t",$value,$key);
	$c++;
}

print Dumper(\@list);

warn("Generating $datadir/data/mlst_schemes");

system("touch $datadir/data/mlst_schemes") or warn "Cannot touch $datadir/data/mlst_schemes: $!\n";

open(OUT,'>',"$datadir/data/mlst_schemes") or warn "GET SCHEMES problem creating list: $!\n";

@list=sort(@list);
print "@list";
print OUT "#scheme\tName\n";
print OUT join("\n",@list), "\n";

close OUT;
system("chmod 775 $datadir/data/mlst_schemes") or warn "Cannot chmod $datadir/data/mlst_schemes: $!\n";
system("chgrp cge $datadir/data/mlst_schemes") or warn "Cannot chgrp $datadir/data/mlst_schemes: $!\n";

system("cp $datadir/data/mlst_schemes $out_dir/mlst_schemes");
#system("cp $datadir/data/mlst_schemes $out_dir/mlst_schemes") or die "Cannot scp datadir/data/mlst_schemes2 cge:/home/data1/services/MLST/mlst_schemes: $!\n";
system("chmod 775 $out_dir/mlst_schemes") or warn "Cannot ssh cge chmod 775 /home/data1/services/MLST/mlst_schemes: $!\n";
system("chgrp cge $out_dir/mlst_schemes") or warn "Cannot ssh cge chgrp cge /home/data1/services/MLST/mlst_schemes: $!\n";

# clean up: ----------------------------------------------------------------------------------------------------------------------------------
#system("rm -f /home/databases/pubmlst/dbases.xml*");

print("DONE! \n");
