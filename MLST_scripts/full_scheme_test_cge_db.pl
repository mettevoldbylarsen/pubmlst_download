#!/usr/local/bin/perl

use strict;
use warnings;

#File with list of all schemes
my $config_file = "/Desktop/repos/mlst_db/config_copy";
open (FIL1, $config_file) or die "Could not open config file $config_file\n";

#File with list of expected ST for each tested organism
my $expected_ST = "/Desktop/goseqittools/local_mlst_db_update_testing/schemes_expected_ST";
open (FIL2, $expected_ST) or die "Could not open file with expected STs\n";
my @expected_ST = <FIL2>;

#Outfile w. results
my $outfile = "/Desktop/goseqittools/local_mlst_db_update_testing/test_results";
open (OUT1, ">$outfile") or die "Could not open outfile\n";

#Declaring variables
my $ided_ST;
my $count_ok = 0;
my $count_problems = 0;

#Looping over each of the schemes
while (defined (my $line = <FIL1>)){
  next if $line =~ m/^#/;
  my @split_line = split("\t",$line);
  my $scheme = $split_line[0];
  my $input_genome = "$scheme" . ".fa";
  #print $input_genome . "\n";
  my $command = "perl \/usr\/src\/cgepipeline\/services\/mlst\/mlst\.pl -d \/Desktop\/repos\/mlst_db -i \/Desktop\/goseqittools\/local_mlst_db_update_testing\/testdata\/" . $input_genome . " -o ". "\/Desktop\/goseqittools\/local_mlst_db_update_testing\/new_run\/" . $scheme . "\/ -s " . $scheme ;
  #print $command . "\n";
  system($command);

  #Finding the identified ST
  my $results_file = "/Desktop/goseqittools/local_mlst_db_update_testing/new_run/" . $scheme . "/results_tab.txt";
  open (FIL3, $results_file) or die "Could not open results file\n";
  while (defined (my $line2 = <FIL3>)){
    if ($line2 =~ m/^Sequence Type/){
      chomp $line2;
      my @split_line2 = split(": ",$line2);
      $ided_ST = $split_line2[1];
    }
  }
  #Loop over expected STs and see if there is a match
  foreach my $elem (@expected_ST){
    chomp $elem;
    next if $elem =~ m/^#/;
    my @split_elem = split("\t", $elem);
    my $scheme_name = $split_elem[0];
    if ($scheme_name eq $scheme){
      my $exp_ST = $split_elem[1];
      if ($exp_ST eq $ided_ST){
        print OUT1 $scheme . "\tExpected: $exp_ST\t" . "Found: $ided_ST\t" . "All OK" . "\n";
        $count_ok += 1;
      } 
      else {
        print OUT1 $scheme . "\tExpected: $exp_ST\t" . "Found: $ided_ST\t" . "PROBLEM" . "\n";
        $count_problems += 1;
      } 
    }
  }
}

print OUT1 "\n#######SUMMARY########\n";
print OUT1 "OK: " . $count_ok . "\n";
print OUT1 "Problems: " . $count_problems . "\n";


close OUT1;